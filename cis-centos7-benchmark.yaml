## Linux Hardening Guide and Checklist
#
# Version 5, December 2018
#
#
### Introduction
#
# This document serves both as a text guide and a YAML spectification intended
# to be used together with goss (https://goss.rocks). It describes the
# requirements for the secure configuration of Linux servers at Acronis. This
# guide is based on
#
# - CIS CentOS Linux 7 Benchmark v2.2.0
# - Red Hat Enterprise Linux 7 STIG
# 
### Requirements

file:
  /bin:
    exists: true
    mode: "0777"
    owner: root
    group: root
    linked-to: usr/bin
    filetype: symlink
    contains: []
  /boot/grub2/grub.cfg:
    exists: true
    mode: "0600"
    owner: root
    group: root
    filetype: file
    contains: []
  /etc/audit/auditd.conf:
    exists: true
    mode: "0640"
    owner: root
    group: root
    filetype: file
    contains: []
  /etc/cron.d:
    exists: true
    mode: "0755"
    owner: root
    group: root
    filetype: directory
    contains: []
  /etc/cron.daily:
    exists: true
    mode: "0755"
    owner: root
    group: root
    filetype: directory
    contains: []
  /etc/cron.hourly:
    exists: true
    mode: "0755"
    owner: root
    group: root
    filetype: directory
    contains: []
  /etc/cron.monthly:
    exists: true
    mode: "0755"
    owner: root
    group: root
    filetype: directory
    contains: []
  /etc/cron.weekly:
    exists: true
    mode: "0755"
    owner: root
    group: root
    filetype: directory
    contains: []
  /etc/crontab:
    exists: true
    mode: "0644"
    owner: root
    group: root
    filetype: file
    contains: []

# V-72269: The operating system must synchronize clocks
#
# TODO: use keys for authentication

  /etc/chrony.conf:
    exists: true
    mode: "0644"
    owner: root
    group: root
    filetype: file
    sha256: 89c7e7ec36ec418629b1b527d6d614afacae5cff11a9632a39aba05529035dd5


  /etc/group:
    exists: true
    mode: "0644"
    owner: root
    group: root
    filetype: file
    contains: []
  /etc/gshadow:
    exists: true
    mode: "0000"
    owner: root
    group: root
    filetype: file
    contains: []

# rsh-related config files must be removed
  /etc/hosts.equiv:
    exists: false

# Only members of the wheel group may use su

  /etc/pam.d/su:
    exists: true
    mode: "0644"
    owner: root
    group: root
    filetype: file
    contains:
      - "/^auth\\s+required\\s+pam_wheel.so\\s+use_uid$/"

# Empty passwords are not permitted
  /etc/pam.d/system-auth:
    exists: true
    contains:
      - "!/nullok/"

  /etc/passwd:
    exists: true
    mode: "0644"
    owner: root
    group: root
    filetype: file
  /etc/rsyslog.conf:
    exists: true
    mode: "0644"
    owner: root
    group: root
    filetype: file

  /etc/security/limits.conf:
    exists: true
    mode: "0644"
    owner: root
    group: root
    filetype: file
    contains:
      - "* hard maxlogins 10"

  /etc/shadow:
    exists: true
    mode: "0000"
    owner: root
    group: root
    filetype: file
    contains: []

#### Requirement XXX
#
# V-72251: The SSH daemon must be configured to only use the SSHv2 protocol. 
# V-71939: The SSH daemon must not allow authentication using an empty password.
# V-71957: The operating system must not allow users to override SSH environment variables.
# V-72263: The SSH daemon must perform strict mode checking of home directory configuration files.
# V-72265: The SSH daemon must use privilege separation.
# V-72247: The system must not permit direct logons to the root account using remote access via SSH.
# V-72243: The SSH daemon must not allow authentication using rhosts authentication.

  /etc/ssh/sshd_config:
    exists: true
    mode: "0600"
    owner: root
    group: root
    filetype: file
    contains:
      - "/^(?i:Protocol)\\s+2\\s*$/"
      - "/^(?i:GSSAPIAuthentication)\\s+no\\s*$/"
      - "/^(?i:KerberosAuthentication)\\s+no\\s*$/"
      - "/^(?i:HostbasedAuthentication)\\s+no\\s*$/"
      - "/^(?i:PermitEmptyPasswords)\\s+no\\s*$/"
      - "/^(?i:X11Forwarding)\\s+no\\s*$/"
      - "/^(?i:MaxAuthTries)\\s+[123456]\\s*$/"
      - "/^(?i:IgnoreRhosts)\\s+yes\\s*$/"
      - "/^(?i:PermitRootLogin)\\s+(?:prohibit-password|without-password|no)\\s*$/"
      - "/^(?i:PermitUserEnvironment)\\s+no\\s*$/"
      - "/^(?i:StrictModes)\\s+yes\\s*$/"
      - "/^(?i:UsePrivilegeSeparation)\\s+sandbox\\s*$/"
      - "/^(?i:ClientAliveInterval)\\s+30\\s*$/"
      - "/^(?i:ClientAliveCountMax)\\s+3\\s*$/"
      - "/^(?i:Compression)\\s+delayed\\s*$/"

#### Requirement XXX
#
# STIG V-72257: The SSH private host key files must have mode 0640 or less permissive.
# STIG V-72255: The SSH public host key files must have mode 0644 or less permissive.

  /etc/ssh/ssh_host_ecdsa_key:
    exists: true
    mode: "0640"
    owner: root
    group: ssh_keys
    filetype: file
  /etc/ssh/ssh_host_ecdsa_key.pub:
    exists: true
    mode: "0644"
    owner: root
    group: root
    filetype: file
  /etc/ssh/ssh_host_ed25519_key:
    exists: true
    mode: "0640"
    owner: root
    group: ssh_keys
    filetype: file
  /etc/ssh/ssh_host_ed25519_key.pub:
    exists: true
    mode: "0644"
    owner: root
    group: root
    filetype: file
  /etc/ssh/ssh_host_rsa_key:
    exists: true
    mode: "0640"
    owner: root
    group: ssh_keys
    filetype: file
  /etc/ssh/ssh_host_rsa_key.pub:
    exists: true
    mode: "0644"
    owner: root
    group: root
    filetype: file


### Requirement XXX
#
# CIS 1.1.1.1: Ensure mounting of cramfs filesystems is disabled
# CIS 1.1.1.2: Ensure mounting of freevxfs filesystems is disabled
# CIS 1.1.1.3: Ensure mounting of jffs2 filesystems is disabled
# CIS 1.1.1.4: Ensure mounting of hfs filesystems is disabled
# CIS 1.1.1.5: Ensure mounting of hfsplus filesystems is disabled
# CIS 1.1.1.6: Ensure mounting of squashfs filesystems is disabled
# CIS 1.1.1.7: Ensure mounting of udf filesystems is disabled
# CIS 1.1.1.8: Ensure mounting of FAT filesystems is disabled
# STIG V-71983: USB mass storage must be disabled.

  /etc/modprobe.d/CIS.conf:
    exists: true
    owner: root
    group: root
    filetype: file
    mode: "0644"
    contains:
      - "install cramfs /bin/true"
      - "install freevxfs /bin/true"
      - "install jffs2 /bin/true"
      - "install hfs /bin/true"
      - "install hfsplus /bin/true"
      - "install squashfs  /bin/true"
      - "install udf /bin/true"
      - "install vfat /bin/true"
      - "install usb-storage /bin/true"

#### Requirement XXX
#
# Disable unused network protocols
#
# DCCP must be disabled
# SCTP must be disabled

  /etc/modprobe.d/net-blacklist.conf:
    exists: true
    owner: root
    group: root
    filetype: file
    mode: "0644"
    contains:
      - "install dccp /bin/false"
      - "install dccp_diag /bin/false"
      - "install dccp_ipv4 /bin/false"
      - "install dccp_ipv6 /bin/false"
      - "install sctp /bin/false"

#### Requirement XXX
# 
# Ensure sudo is configured

  /etc/sudoers:
    exists: true
    mode: "0440"
    owner: root
    group: root
    filetype: file
    contains: []

#### Requirement XXX
#
# Ensure gpgcheck Enabled for Repository Metadata
# Ensure gpgcheck Enabled In Main Yum Configuration
# Ensure YUM Removes Previous Package Versions

  /etc/yum.conf:
    exists: true
    mode: "0644"
    owner: root
    group: root
    filetype: file
    contains:
    - /^\s*gpgcheck\s*=\s*1\s*$/
    - /^\s*repo_gpgcheck\s*=\s*1\s*$/
    - /^\s*localpkg_gpgcheck\s*=\s*1\s*$/
    - /^\s*clean_requirements_on_remove\s*=\s*1\s*$/

  /lib:
    exists: true
    mode: "0777"
    owner: root
    group: root
    linked-to: usr/lib
    filetype: symlink
    contains: []

  /lib64:
    exists: true
    mode: "0777"
    owner: root
    group: root
    linked-to: usr/lib64
    filetype: symlink
    contains: []

  /sbin:
    exists: true
    mode: "0777"
    owner: root
    group: root
    linked-to: usr/sbin
    filetype: symlink
    contains: []

  /tmp:
    exists: true
    mode: "1777"
    owner: root
    group: root
    filetype: directory

  /usr/bin:
    exists: true
    mode: "0555"
    owner: root
    group: root
    filetype: directory
    contains: []

  /usr/lib:
    exists: true
    mode: "0555"
    owner: root
    group: root
    filetype: directory
    contains: []

  /usr/lib64:
    exists: true
    mode: "0555"
    owner: root
    group: root
    filetype: directory
    contains: []

  /usr/local/bin:
    exists: true
    mode: "0755"
    owner: root
    group: root
    filetype: directory
    contains: []

  /usr/local/sbin:
    exists: true
    mode: "0755"
    owner: root
    group: root
    filetype: directory
    contains: []

  /usr/sbin:
    exists: true
    mode: "0555"
    owner: root
    group: root
    filetype: directory
    contains: []

  /var/log/audit:
    exists: true
    mode: "0700"
    owner: root
    group: root
    filetype: directory
    contains: []

  /var/log/audit/audit.log:
    exists: true
    mode: "0600"
    owner: root
    group: root
    filetype: file
    contains: []

package:

# CIS 1.1.22: Disable Automounting

  autofs:
    installed: false

  avahi:
    installed: false
  cups:
    installed: false
  dhcp:
    installed: false
  gdb:
    installed: false
  net-snmp:
    installed: false
  rsh:
    installed: false
  rsh-server:
    installed: false
  rsyslog:
    installed: true
  samba:
    installed: false
  samba-client:
    installed: false
  sendmail:
    installed: false
  sudo:
    installed: true
  talk:
    installed: false
  talk-server:
    installed: false
  telnet-server:
    installed: false
  tftp:
    installed: false
  tftp-server:
    installed: false
  xinetd:
    installed: false
  yp-tools:
    installed: false
  ypbind:
    installed: false
  ypserv:
    installed: false

### Requirement XXX
#
# Unused services must be disabled.
#
# V-72057 Kernel core dumps must be disabled unless needed

service:
  abrtd:
    enabled: false
    running: false
  auditd:
    enabled: true
    running: true

# CIS 1.1.22: Disable Automounting

  autofs:
    enabled: false
    running: false

  avahi-daemon:
    enabled: false
    running: false
  bluetooth:
    enabled: false
    running: false
# V-72269: The operating system must synchronize clocks
  chronyd:
    enabled: true
    running: true
  crond:
    enabled: true
    running: true
  kdump:
    enabled: false
    running: false
  oddjobd:
    enabled: false
    running: false
  qpidd:
    enabled: false
    running: false
  rdisc:
    enabled: false
    running: false
  rlogin:
    enabled: false
    running: false
  rexec:
    enabled: false
    running: false
  rsh:
    enabled: false
    running: false
  rsyslog:
    enabled: true
    running: true
  snmpd:
    enabled: false
    running: false
  sshd:
    enabled: true
    running: true
  telnet:
    enabled: false
    running: false
  tftp:
    enabled: false
    running: false
  xinetd:
    enabled: false
    running: false
  ypbind:
    enabled: false
    running: false

command:
# check /etc/password and /etc/shadow consistency
  pwck -rq:
    exit-status: 0
    stdout: []
    stderr: []
    timeout: 0

# verify files integrity via rpm
# TODO: "nomode" option to be removed

  rpm -Va --noconfig --nomode:
    exit-status: 0
    timeout: 30000

### Requirement XXX
#
# Kernel parameters must be configured for security
#
kernel-param:
  fs.suid_dumpable:
    value: "0"
  kernel.randomize_va_space:
    value: "2"
  net.ipv4.conf.all.send_redirects:
    value: "0"
  net.ipv4.conf.all.accept_redirects:
    value: "0"
  net.ipv4.conf.all.accept_source_route:
    value: "0"
  net.ipv4.conf.all.secure_redirects:
    value: "0"
  net.ipv4.conf.all.log_martians:
    value: "1"
  net.ipv4.icmp_echo_ignore_broadcasts:
    value: "1"
  net.ipv4.icmp_ignore_bogus_error_responses:
    value: "1"
  net.ipv4.tcp_syncookies:
    value: "1"
  net.ipv4.conf.all.rp_filter:
    value: "1"
  net.ipv4.tcp_max_syn_backlog:
    value: "4096"
  net.ipv4.ip_forward:
    value: "0"
  net.ipv4.conf.all.log_martians:
    value: "1"
  net.ipv4.tcp_max_orphans:
    value: "4096"



mount:


# CIS 1.1.2: Ensure separate partition exists for /tmp
# CIS 1.1.3: Ensure nodev option set on /tmp partition
# CIS 1.1.4: Ensure nosuid option set on /tmp partition
# CIS 1.1.5: Ensure noexec option set on /tmp partition
#
# NOTE: also see https://access.redhat.com/solutions/1340503

  /tmp:
    exists: true
    opts:
    - nodev
    - nosuid
    - noexec

# CIS 1.1.6: Ensure separate partition exists for /var
  /var:
    exists: true

# CIS 1.1.7: Ensure separate partition exists for /var/tmp
# CIS 1.1.8: Ensure nodev option set on /var/tmp partition
# CIS 1.1.9: Ensure nosuid option set on /var/tmp partition
# CIS 1.1.10: Ensure noexec option set on /var/tmp partition

  /var/tmp:
    exists: true
    opts:
    - nodev
    - nosuid
    - noexec

# CIS 1.1.11: Ensure separate partition exists for /var/log

  /var/log:
    exists: true

# CIS 1.1.12: Ensure separate partition exists for /var/log/audit

  /var/log/audit:
    exists: true

# CIS 1.1.13 Ensure separate partition exists for /home
# CIS 1.1.14 Ensure nodev option set on /home partition

  /home:
    exists: true
    opts:
      - nodev

# CIS 1.1.15 Ensure nodev option set on /dev/shm partition 
# CIS 1.1.16 Ensure nosuid option set on /dev/shm partition
# CIS 1.1.17 Ensure noexec option set on /dev/shm partition

  /dev/shm:
    exists: true
    opts:
    - nodev
    - nosuid
    - noexec

# TODO:
# 
# CIS 1.1.18: Ensure nodev option set on removable media partitions
# CIS 1.1.19: Ensure nosuid option set on removable media partitions
# CIS 1.1.20: Ensure noexec option set on removable media partitions
# CIS 1.1.21: Ensure sticky bit is set on all world-writable directories
# CIS 1.2.1: Ensure package manager repositories are configured
