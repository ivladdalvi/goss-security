# Security configuration validation with goss

This is an attempt to automate validation against [CIS
Benchmark](https://www.cisecurity.org/cis-benchmarks/) with
[goss](https://github.com/aelsabbahy/goss) testing framework. It is based on CIS
CentOS Linux 7 Benchmark version v2.2.0.

## Benefits

Why not to use [OpenSCAP](https://www.open-scap.org/) for security testing.
There are couple of reasons.  First, `goss` is *fast*: the full test set except
for `rpm -Va` validation completes under 1 second on a smallest Dropbox droplet
(1 VCPU, 25 GB SSD).  Same checklist written in
[serverspec](http://serverspec.org/) will probably take a few minutes.  Second,
XCCDF is XML, it is difficult to work with, especially if you want to produce a
self-documenting code. Third, unlike OpenSCAP, with `goss` you can create HTTP
endpoint to report validation results and integrate it into your existing
monitoring process. Fourth, unlike serverspec, `goss` is just a single static
binary with no dependencies.

Also, since `goss` could be used for a generic server config validation, it is
easier to integrate security checks into existing pipeline.

## Usage

Clone the repo. Get `goss` from https://github.com/aelsabbahy/goss/. Then, run

```
goss --gossfile lhcl.yaml validate 
``` 

for a one-time check, or

```
goss --gossfile lhcl.yaml serve --format json
```

to get JSON-formatted results on `http://your.server.ip:8080/healthz`. For more
information, check [goss
documentation](https://github.com/aelsabbahy/goss/blob/master/docs/manual.md).


## License

This work is licensed under a [Creative Commons
Attribution-NonCommercial-ShareAlike 4.0 International Public
License](https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode).
